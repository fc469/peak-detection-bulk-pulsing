# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 12:08:45 2019

@author: Filip
"""

import os
import pandas as pd

path = os.path.dirname(os.path.realpath(__file__))
filelist = os.listdir(path)

for file in filelist:
    if file[-5:]=='.xlsx':
        data_xls = pd.read_excel(path + '/' + file, index_col=None)
        data_xls.to_csv(path + '/' + file[:-4] + '.csv', index = False, encoding='utf-8')
    else:
        continue
    