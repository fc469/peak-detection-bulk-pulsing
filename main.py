# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 09:57:58 2019

@author: Filip
"""
#######################################################
#General usage:
#Copy the .py file into the folder with .xls data
#All files will be renamed to .txt
#Modified data will be saved as .xls
#.pdf file with peak detection plots will be used as Pulse_Detection_Plots.pdf
#also in this folder
#######################################################

from scipy import signal
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import os
import copy

def single_peak_filter(data):
    for i in range(len(data)-2):
        if abs((data[i]-data[i+2])/data[i]) < 0.05 and abs((data[i]-data[i+1])/data[i])>0.1:
            data[i+1] = (data[i] + data[i+2])/2
    return data

def butter_filter(data):
    b, a = signal.butter(3, 0.05)
    z = signal.filtfilt(b, a, data)
    return z

path = os.path.dirname(os.path.realpath(__file__))
filelist = os.listdir(path)

#Renaming files
for file in filelist:
    if file[-4:]=='.xls':
        os.rename(path + '/' + file, path + '/' + file[:-4] + '.txt')
#    elif file[-5:]=='.xlsx':
#        os.rename(path + '/' + file, path + '/' + file[:-5] + '.csv')
    else:
        continue

filelist = os.listdir(path)
filelist = filelist[1:]

#Prep for pdf document output
pdf = matplotlib.backends.backend_pdf.PdfPages(path + "/" + "Pulse_Detection_Plots.pdf")
offset = 0

for item in filelist[0:1]:
    
    if item[-4:] == '.txt' or item[-4:] == '.csv':
        filename = path + '/' + item
        
        f=open(filename,"r")
        lines=f.readlines()
        f.close()
        
        #Timestep
        dt=float(lines[2].split()[2])
        
        lines = lines[5:]

        v = np.zeros(len(lines))
        h1 = np.zeros(len(lines))
        h2 = np.zeros(len(lines))
        h3 = np.zeros(len(lines))
        h4 = np.zeros(len(lines))
        h5 = np.zeros(len(lines))
        
        #load arrays from text file
        for i in range(len(lines)):
            v[i],h1[i],h2[i],h3[i],h4[i],h5[i] = lines[i].split()[2:]
            
        #Flipping and multiplying
        h1*=-5
        h2*=-5
        h3*=-5
        h4*=-5
        h5*=-5
        v*=100
        
        #Adjust sensor readings
        if offset == 0:
            offset = np.average(h1[0:500])
            input("Offset used for all Hall sensor readings is " + str(offset) + " " + "Write OK and press ENTER")
        
        h1-=offset
        h2-=offset
        h3-=offset
        h4-=offset
        h5-=offset
        
        #copying
        v_copy = copy.deepcopy(v)
        h1_copy = copy.deepcopy(h1)
        h2_copy = copy.deepcopy(h2)
        h3_copy = copy.deepcopy(h3)
        h4_copy = copy.deepcopy(h4)
        h5_copy = copy.deepcopy(h5)

        #Single peak filter
        v = single_peak_filter(v)
        h1 = single_peak_filter(h1)
        h2 = single_peak_filter(h2)
        h3 = single_peak_filter(h3)
        h4 = single_peak_filter(h4)
        h5 = single_peak_filter(h5)
        
        #find index of rapid change
        
        #from voltage
        v_filtered = butter_filter(v)
        v_filtered -=v_filtered[0]
        index1 = np.argmax(v_filtered<-10)
        #-3 before
        
        #from sensor
        h_filtered = butter_filter(h1)
        h_filtered -=h_filtered[0]
        index2 = np.argmax(h_filtered>0.01)
        
        if index1<index2:
            index = index1
        else:
            index = index2
        #Filter 2nd time, smaller range
        #from voltage
        half_int = 100
        if index-half_int>=0 and index+half_int<len(v):
            v_filtered = butter_filter(v[index-half_int:index+half_int])
            v_filtered -=v_filtered[0]
            index1 = np.argmax(v_filtered<-0.25) + index -half_int
            
            #Filter overshhot
            if v_filtered[index1 - index + half_int + 20]>v_filtered[index1 - index + half_int]:
                #Generate index1 from only v
                index1 = np.argmax((v[index-half_int:index+half_int]-v[index-half_int])<-5) + index -half_int
            
            #From sensor
            h_filtered = butter_filter(h1[index-half_int:index+half_int])
            h_filtered -=h_filtered[0]
            index2 = np.argmax(h_filtered>0.004) + index -half_int
            
            if index1<index2:
                index = index1
            else:
                index = index2
        #Export to xlsx
        t = np.arange(0,len(v[index:])*dt,dt)
        xls_layout = {'time': t, 'voltage0' : v_copy[index:], 'voltage1' : h1_copy[index:], 'voltage2' : h2_copy[index:],
                      'voltage3' : h3_copy[index:], 'voltage4' : h4_copy[index:], 'voltage5' : h5_copy[index:]}
        df = pd.DataFrame.from_dict(xls_layout)
        df.to_excel(filename[:-4] + '_mod'+ '.xlsx', header=True, index=False)
    
        #Cutt-of plotting
        
        #Plot
        fig = plt.figure()
        inter = 500
        plt.plot(h1[index-inter:index+inter],linewidth=0.5)
        plt.plot((v[index-inter:index+inter])/100,linewidth=0.5)
#       plt.plot(butter_filter(h1[index-inter:index+inter]))
#       plt.plot(butter_filter(v[index-inter:index+inter])/100)
        
        plt.axvline(x=inter,color = 'r',linewidth=0.5)
        plt.title(item)
        pdf.savefig(fig)
        
    else:
        continue

pdf.close()

#Testing snippets
#        print(h_filtered[np.argmax(h_filtered>0.002)])
#        plt.plot(h_filtered)
#        plt.axvline(np.argmax(h_filtered>0.002),color = 'r')
#        plt.show()


